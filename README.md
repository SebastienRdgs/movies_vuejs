Projet films vuejs
========================

### Clone du git :
```
git clone https://github.com/SebastienRdgs/movies_vuejs.git
```

### Installation des dépendances et lancement du serveur webpack :
```
npm install && node_modules/.bin/webpack --progress --hide-modules --mode=development && node src/node/app.js
```

* Naviguez entre les différents liens et bouttons pour voir l'ensemble des fonctionnalités
