var express = require('express')
var path = require('path')
var router = express.Router()
const bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next()
})

// Route static (CSS, PICTURES)
router.use('/dist', express.static(path.join(__dirname + '/../../dist')))
router.use('/static', express.static(path.join(__dirname + '/../static')))

// Route Index
router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/../../index.html'))
})

// All movies
router.get('/api/movies/all', (req, res) => {
    res.json(movieslist)
})

// New movie
router.post('/api/movie', (req, res) => {
	let movie = req.body;
    let id = movieslist[movieslist.length-1].id + 1;
    movie['id'] = id;
    movieslist.push(movie);
    res.json(movie);
})

// Edit movie
router.post('/api/movies/:id', (req, res) => {
	let id = parseInt(req.params.id);
    let updateMovie = req.body;
    movie = movieslist.find(m => m.id === id);
    
    movie.title = updateMovie.title;
    movie.synopsys = updateMovie.synopsys;
    movie.year = updateMovie.year;
    movie.lang = updateMovie.lang;
    movie.genre = updateMovie.genre;
    movie.producer.name = updateMovie.producer.name;
    movie.producer.nationality = updateMovie.producer.nationality;
    movie.producer.birth = updateMovie.producer.birth;
})

// Delete movie
router.get('/api/delete/:id', (req, res) => {
	movieslist.splice(parseInt(req.params.id)-1, 1);
    res.send(movieslist);
})

// Mark movie
router.post('/api/movies/:id/mark/:mark', function(req, res){
    let id = parseInt(req.params.id);
    let movie = movieslist.find(m => m.id === id);
    movie.mark = req.params.mark;
    res.send(movie);
})

module.exports = router