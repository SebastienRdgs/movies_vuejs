const express = require('express')
const routes = require('./routes.js')
const app = express();

app.use('/', routes)

app.listen(3000, () => console.log('App running : http://localhost:3000'))

global.movieslist = [
    {
    	id: 1,
        title: "L'étrange noël de M. Jack",
        year: 1994,
        lang: "FR",
        producer: {
        	name: "Tim Burton",
        	nationality: "US",
        	birth: "25/08/1958"
        },
        genre: "Film d'animation, horreur",
        synopsys: "Jack Skellington, un épouvantail squelettique surnommé « le Roi des citrouilles » (Pumpkin King en version originale), vit dans la ville d'Halloween. En tant que maître de l'épouvante, Jack occupe ses journées à préparer la prochaine fête d'Halloween.",
        mark: 4.5,
        poster: "/static/nightmare.png"
    },
    {
    	id: 2,
        title: "Interstellar",
        year: 2014,
        lang: "FR",
        producer: {
        	name: "Christopher Nolan",
        	nationality: "US",
        	birth: "30/07/1970"
        },
        genre: "Science fiction",
        synopsys: "Alors que la Terre se meurt, une équipe d'astronautes franchit un trou de ver apparu près de Saturne conduisant à une autre galaxie, cela dans le but d'explorer un nouveau système stellaire et l'espoir de trouver une nouvelle planète habitable par l'humanité afin de la sauver.",
        mark: 4,
        poster: "/static/interstellar.jpg"
    }
]