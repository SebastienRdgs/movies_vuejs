import VueRouter from 'vue-router'
import Vue from 'vue'
import movieList from './components/movielist.vue'
import movieEdit from './components/movieedit.vue'
import movieItem from './components/movieitem.vue'
import movieAdd from './components/movieadd.vue'

Vue.use(VueRouter)

const routes = [
	{path: '/', name: 'movielist', component: movieList},
	{path: '/movie/:id/edit', name: 'movieedit', component: movieEdit},
	{path: '/movie/:id', name: 'movieitem', component: movieItem},
	{path: '/movie/add', name: 'movieadd', component: movieAdd}
]

export default new VueRouter({
	routes
})