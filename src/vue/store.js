import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
        movies : []
  	},
    actions: {
        newMovie (state, movie_to_add) {
            axios({
                method: 'post',
                url: 'http://localhost:3000/api/movie',
                responseType : 'JSON',
                data : movie_to_add
            })
            .then(function(response){
                state.commit('newMovie', response.data);
            });
        },
        editMovie (state, movie_to_edit) {
            let id = movie_to_edit.id;
            axios({
                method: 'post',
                url: 'http://localhost:3000/api/movies/'+id,
                responseType : 'JSON',
                data : movie_to_edit
            })
            .then(function(response){
                state.commit('editMovie', response.data);
            });
        },
        setMovies (context) {
            axios({
                method: 'get',
                url: 'http://localhost:3000/api/movies/all',
                responseType : 'JSON'
            })
            .then(function(response){
                context.commit('setMovies', response.data);
            });
        },
        deleteMovie (state, movieId) {
            axios({
                method: 'get',
                url: 'http://localhost:3000/api/delete/'+movieId,
                responseType : 'JSON'
            })
            .then(function(response){
                state.commit('deleteMovie', movieId);
            });
        },
        setMovie (state, movie) {
            axios({
                method: 'post',
                url: 'http://localhost:3000/api/movies/'+movie.id+'/mark/'+movie.mark,
                responseType : 'JSON'
            })
            .then(function(response){
                state.commit('setMovie', response.data);
            });
        }
    },
  	mutations: {
    	newMovie (state, movie_to_add) {
            state.movies.push(movie_to_add);
        },
        editMovie (state, movie_to_edit) {
            state.movies = movie_to_edit;
        },
        setMovies (state, movies) {
            state.movies = movies;
        },
        deleteMovie (state, movieId) {
            state.movies.splice(movieId,1);
        },
        setMovie (state, movie) {
            state.movies = movie;
        },
  	}
})