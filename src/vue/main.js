import Vue from 'vue' //librairie "vue" dans node_modules
import app from './app.vue' //fichier app.vue local
import router from './routes.js'
import store from './store.js'

import movieItem from './components/movieitem.vue'
Vue.component('movie-item', movieItem)

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(app)
})